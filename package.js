Package.describe({
    name: 'accounts-password',
    version: '1.6.0',
    summary: 'Accounts system for Cloudron',
    git: 'https://github.com/cloudron-io/accounts-cloudron',
    documentation: 'README.md'
});

Npm.depends({
    "ldapjs": "1.0.0"
});

Package.onUse(function(api) {
    api.use('ecmascript');
    api.use('http');

    api.use('accounts-base', ['client', 'server']);

    api.imply('accounts-base', ['client', 'server']);

    api.addFiles('cloudron-templates.js', 'server');
    api.addFiles('cloudron-server.js', 'server');
    api.addFiles('cloudron-client.js', 'client');
});
