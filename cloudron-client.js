
Meteor.loginWithPassword = function (selector, password, callback) {

    if (typeof selector === 'string') {
        if (selector.indexOf('@') === -1) {
            selector = { username: selector };
        } else {
            selector = { email: selector };
        }
    }

    Accounts.callLoginMethod({
        methodArguments: [{
            user: selector,
            password: password
        }],
        userCallback: callback
    });
};

Accounts.createUser = function (options, callback) {
    callback(new Error('Create user not supported'));
};

Accounts.changePassword = function (oldPassword, newPassword, callback) {
    callback(new Error('Changing password not supported'));
};

Accounts.forgotPassword = function(options, callback) {
    callback(new Error('Forgot password not supported'));
};

Accounts.resetPassword = function(token, newPassword, callback) {
    callback(new Error('Reset password not supported'));
};

Accounts.verifyEmail = function(token, callback) {
    callback(new Error('Verify email not supported'));
};
